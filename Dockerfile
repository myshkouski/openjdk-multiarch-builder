ARG JDK_DISTRO="temurin-21"

################################################################################
# STAGE 1: setup builders
################################################################################

#  Kotlin/Native prebuilt compiler is available for x86_64 Linux only, so Use "linux/amd64" platform for base builder image
FROM --platform=linux/amd64 debian:bookworm-backports AS debian-builder-base
ARG JDK_DISTRO
RUN \
    --mount=type=cache,sharing=locked,target=/var/lib/apt/lists \
    --mount=type=cache,sharing=locked,target=/var/cache/apt \
    apt-get update && \
    apt-get install --no-install-recommends -y \
        ca-certificates \
        git \
        cmake \
        pkgconf \
        build-essential
RUN \
    --mount=type=cache,sharing=locked,target=/var/lib/apt/lists \
    --mount=type=cache,sharing=locked,target=/var/cache/apt \
    apt-get install --no-install-recommends -y \
        wget \
        apt-transport-https \
        gpg
RUN \
    wget -qO - "https://packages.adoptium.net/artifactory/api/gpg/key/public" | gpg --dearmor | tee /etc/apt/trusted.gpg.d/adoptium.gpg > /dev/null && \
    echo "deb https://packages.adoptium.net/artifactory/deb $(awk -F= '/^VERSION_CODENAME/{print$2}' /etc/os-release) main" | tee /etc/apt/sources.list.d/adoptium.list
RUN \
    --mount=type=cache,sharing=locked,target=/var/lib/apt/lists \
    --mount=type=cache,sharing=locked,target=/var/cache/apt \
    apt-get update && \
    apt-get install --no-install-recommends -y \
        ${JDK_DISTRO}-jdk

FROM debian-builder-base AS debian-builder-base-amd64
ENV KOTLIN_NATIVE_TARGET_PLATFORM="linuxX64"

FROM debian-builder-base AS debian-builder-base-arm64
RUN \
    dpkg --add-architecture arm64
RUN \
    --mount=type=cache,sharing=locked,target=/var/lib/apt/lists \
    --mount=type=cache,sharing=locked,target=/var/cache/apt \
    apt-get install --no-install-recommends -y \
        crossbuild-essential-arm64
ENV KOTLIN_NATIVE_TARGET_PLATFORM="linuxArm64"

FROM debian-builder-base-${TARGETARCH} AS debian-builder-multiarch

FROM debian-builder-multiarch
